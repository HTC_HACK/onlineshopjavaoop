import product.BuyProducts;
import product.User;

import java.util.Scanner;

public class Main {

    public static void main(String[] args) {
        buyProducts();
    }

    private static void buyProducts() {
        boolean active = true;
        BuyProducts buyProducts = new BuyProducts();
        Scanner adminL = new Scanner(System.in);
        while (active) {
            System.out.println("1.Admin login.\n2.SignIn.\n3.SignUp.\n0.Exit");
            System.out.print("Enter option: ");
            int option = new Scanner(System.in).nextInt();

            switch (option) {
                case 1: {
                    User user = buyProducts.signIn();
                    if (user != null && user.isAdmin() == true) {
                        boolean adminMenu = true;
                        while (adminMenu) {
                            System.out.println("1.Add product\n2.User List\n3.Product List.\n4.Add quantity exist Product\n5.User List By Product.\n6.Balance.\n7.Logout");
                            int adminOption = new Scanner(System.in).nextInt();
                            switch (adminOption) {
                                case 1: {
                                    buyProducts.addProducts(user);
                                    break;
                                }
                                case 2: {
                                    buyProducts.userList(user);
                                    break;
                                }
                                case 3: {
                                    buyProducts.productList(user);
                                    break;
                                }
                                case 4: {
                                    buyProducts.productEdit(user);
                                    break;
                                }
                                case 5: {
                                    buyProducts.historyUser();
                                    break;
                                }
                                case 6: {
                                    System.out.println("Balance : " + user.getBalance());
                                    break;
                                }
                                case 7: {
                                    adminMenu = false;
                                    break;
                                }
                            }
                        }
                    } else {
                        System.out.println("Wrong password or phone");
                    }
                    break;
                }
                case 2: {
                    User user = buyProducts.signIn();
                    if (user != null && user.isAdmin() == false) {
                        boolean menuActiove = true;
                        while (menuActiove) {
                            System.out.println("1.Buy products\n2.Bought items\n3.Balance.\n4.Add Balance.\n5.Add Plastic Card.\n6.Logout");
                            int menuOption = new Scanner(System.in).nextInt();
                            switch (menuOption) {
                                case 1: {
                                    buyProducts.buyProducts(user);
                                    break;
                                }
                                case 2: {
                                    buyProducts.historyList(user);
                                    break;
                                }
                                case 3: {
                                    System.out.println("Balance     : " + user.getBalance());
                                    System.out.println("Card number : " + user.getCardNumber());
                                    break;
                                }
                                case 4: {
                                    user.addBalance(user);
                                    break;
                                }
                                case 5: {
                                    user.addPlasticCard(user);
                                    break;
                                }
                                case 6: {
                                    menuActiove = false;
                                    break;
                                }
                                default:
                                    System.out.println("Wrong option");
                            }
                        }
                    } else {
                        System.out.println("User not found!");
                    }
                    break;
                }
                case 3: {
                    buyProducts.signUp();
                    break;
                }
                case 0: {
                    active = false;
                    break;
                }
                default: {
                    System.out.println("Wrong option");
                }

            }
        }
    }
}
