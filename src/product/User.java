package product;

import java.util.Scanner;

public class User {
    private Integer id;
    private String name;
    private String phone;
    private String password;
    private String cardNumber;
    private double balance = 15000;
    private boolean admin;


    public void addPlasticCard(User user) {
        int min = 10000;
        int max = 99999;
        System.out.print("Enter card number : ");
        String number = new Scanner(System.in).nextLine();
        int b = (int) (Math.random() * (max - min + 1) + min);
        System.out.println("Verification code " + b);
        System.out.print("Enter verification code : ");
        int verifyNum = new Scanner(System.in).nextInt();

        if (verifyNum == b) {
            user.setCardNumber(number);
            System.out.println("Crad added successfully");
        } else {
            System.out.println("Verification failed");
        }

    }

    public void addBalance(User user) {
        System.out.print("Enter amount : ");
        double amount = new Scanner(System.in).nextDouble();
        if (amount > 0) {
            user.setBalance(user.getBalance() + amount);
            System.out.println("Added Successfully");
        } else {
            System.out.println("Invalid amount");
        }
    }

    public double reduceBalance(double amount) {
        this.balance -= amount;
        return this.balance;
    }

    public String getPassword() {
        return password;
    }

    public void setPassword(String password) {
        this.password = password;
    }

    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public String getPhone() {
        return phone;
    }

    public void setPhone(String phone) {
        this.phone = phone;
    }

    public double getBalance() {
        return balance;
    }

    public void setBalance(double balance) {
        this.balance = balance;
    }

    public boolean isAdmin() {
        return admin;
    }

    public void setAdmin(boolean admin) {
        this.admin = admin;
    }

    public String getCardNumber() {
        return cardNumber;
    }

    public void setCardNumber(String cardNumber) {
        this.cardNumber = cardNumber;
    }
}
