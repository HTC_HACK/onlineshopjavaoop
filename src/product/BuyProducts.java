package product;

import product.User;
import product.Product;

import java.util.Scanner;

public class BuyProducts {

    User[] users = new User[0];

    Product[] products = new Product[0];

    History[] histories = new History[0];

    int userId = 1;
    int productId = 1;
    int historyId = 1;

    //admin

    public BuyProducts() {
        User admin = new User();
        admin.setId(userId);
        admin.setName("admin");
        admin.setPhone("1005060");
        admin.setPassword("123456");
        admin.setAdmin(true);
        admin.setBalance(5000);
        userId++;

        User[] userscopy = users;

        users = new User[userscopy.length + 1];
        System.arraycopy(userscopy, 0, users, 0, userscopy.length);
        users[userscopy.length] = admin;
    }

    public boolean checkPhone(String phone) {
        for (User user : users) {
            if (user.getPhone().equals(phone)) {
                return false;
            }
        }

        return true;
    }

    public void userList(User user) {
        if (user.isAdmin()) {
            System.out.println("|-----------------------------");
            for (User user1 : users) {
                System.out.println("| Name : " + user1.getName());
                System.out.println("| Role : " + (user1.isAdmin() == false ? "customer" : "admin"));
                System.out.println("|-----------------------------");
            }
        }
    }

    public void productList(User user) {
        if (user.isAdmin()) {
            System.out.println("|----------------------------------");
            for (Product product : products) {
                System.out.println("| Product id       : " + product.getId());
                System.out.println("| Product name     : " + product.getName());
                System.out.println("| Product price    : " + product.getPrice());
                System.out.println("| Product quantity : " + product.getQuantity());
                System.out.println("|----------------------------------");
            }
        }
    }

    public void historyList(User user1) {
        System.out.println("|---------------------");
        for (History history : histories) {
            if (history.getUserId() == user1.getId()) {
                System.out.println("| Product name     : " + history.getName());
                System.out.println("| Product quantity : " + history.getQuantity());
                System.out.println("| Total price      : " + (history.getPrice() * history.getQuantity()));
                System.out.println("|---------------------");
            }
        }
    }

    public void historyUser() {
        System.out.println("|--------------------------------");

        for (User user : users) {
            for (History history : histories) {
                if (user.getId() == history.getUserId()) {
                    System.out.println("| User name    : " + user.getName());
                    System.out.println("| Product name : " + history.getName());
                    System.out.println("| Buy quantity : " + history.getQuantity());
                    System.out.println("| Buy price    : " + history.getPrice());
                    System.out.println("| Total        : " + (history.getQuantity() * history.getPrice()));
                    System.out.println("|--------------------------------");
                }
            }
        }
    }

    public void addProducts(User user) {

        Product product = new Product();
        Scanner scanner = new Scanner(System.in);
        Scanner quantityInput = new Scanner(System.in);

        if (user.isAdmin()) {
            System.out.print("Enter product name: ");
            String name = scanner.nextLine();
            product.setName(name);

            System.out.print("Enter product quantity: ");
            double quantity = quantityInput.nextDouble();
            product.setQuantity(quantity);

            System.out.print("Enter product price: ");
            double price = quantityInput.nextDouble();
            product.setPrice(price);

            product.setId(productId);
            productId++;

            product.setUserId(1);

            Product[] productscopy = products;
            products = new Product[productscopy.length + 1];
            System.arraycopy(productscopy, 0, products, 0, productscopy.length);
            products[productscopy.length] = product;

            System.out.println("Product added successfully");
        }


    }

    public void productEdit(User user) {
        productList(user);
        Product product = new Product();
        Scanner scanner = new Scanner(System.in);
        Scanner quantityInput = new Scanner(System.in);
        System.out.print("Enter id: ");
        int pro = quantityInput.nextInt();

        for (Product product1 : products) {
            if (product1.getId() == pro) {
                System.out.println("How much: ");
                double amount = quantityInput.nextDouble();
                product1.addQuantity(amount);

                System.out.println("Quantity successfully added");
                break;

            }
        }
    }


    //User

    public void signUp() {
        User user = new User();
        Scanner scanner = new Scanner(System.in);

        System.out.print("Enter name: ");
        String name = scanner.nextLine();
        user.setName(name);

        System.out.print("Enter phone number: ");
        String phone = scanner.nextLine();
        if (checkPhone(phone)) {
            user.setPhone(phone);

            System.out.print("Enter password: ");
            String password = scanner.nextLine();
            user.setPassword(password);

            user.setId(userId);
            userId++;

            User[] userscopy = users;

            users = new User[userscopy.length + 1];
            System.arraycopy(userscopy, 0, users, 0, userscopy.length);
            users[userscopy.length] = user;

            System.out.println("Successfully created account");
        } else {
            System.out.println("Phone already exist");
        }

    }

    public User signIn() {
        User user = new User();
        Scanner scanner = new Scanner(System.in);
        System.out.println("Enter phone number: ");
        String phone = scanner.nextLine();
        user.setPhone(phone);
        System.out.println("Enter password: ");
        String password = scanner.nextLine();
        user.setPassword(password);

        for (User user1 : users) {

            if (user.getPhone().equals(user1.getPhone())) {
                if (user.getPassword().equals(user1.getPassword())) {
                    return user1;
                }
            }
        }

        return null;
    }

    public void buyProducts(User user) {
        int countProduct = 0;
        for (Product product : products) {
            if (product != null) {
                countProduct++;
            }
        }

        if (countProduct == 0) {
            System.out.println("No product yet");
        } else {
            System.out.println("|-------------------------");
            for (Product product : products) {
                if (product != null) {
                    System.out.println("| Product id     : " + product.getId());
                    System.out.println("| Product name   : " + product.getName());
                    System.out.println("| Product price  : " + product.getPrice());
                    System.out.println("| Available      : " + product.getQuantity());
                    System.out.println("|-------------------------");
                }
            }
            Scanner scanner = new Scanner(System.in);
            System.out.println("Enter option: ");
            int option = scanner.nextInt();

            System.out.println("How much: ");
            double quantity = scanner.nextInt();

            if (checkBalance(user, quantity, option)) {

                History history = new History();
                history.setUserId(userId);
                history.setProductId(option);
                history.setQuantity(quantity);

                for (Product product : products) {
                    if (product.getId() == option) {
                        history.setPrice(product.getPrice());
                        history.setName(product.getName());
                        double total = quantity * product.getPrice();
                        addBalanceAdmin(total);
                        product.reduceQuantity(quantity);
                        user.reduceBalance(quantity * product.getPrice());

                        history.setUserId(user.getId());
                        history.setId(historyId);
                        historyId++;

                        History[] historiescopy = histories;
                        histories = new History[historiescopy.length + 1];
                        System.arraycopy(historiescopy, 0, histories, 0, historiescopy.length);
                        histories[historiescopy.length] = history;

                        break;
                    }
                }
            } else {
                System.out.println("Check your balance!");
            }
        }


    }

    private void addBalanceAdmin(double total) {
        for (User user : users) {
            if (user.isAdmin() == true) {
                user.setBalance(user.getBalance() + total);
                break;
            }
        }
    }

    public boolean checkBalance(User user, double quantity, int productId) {

        for (Product product : products) {
            if (product.getId() == productId) {
                if (quantity * product.getPrice() <= user.getBalance()) {
                    return true;
                } else {
                    return false;
                }
            }
        }

        return false;
    }

}












