package product;

public class Product {
    private Integer id;
    private String name;
    private double price;
    private double quantity;
    private Integer userId;


    public double reduceQuantity(double amount){
        this.quantity -= amount;
        return this.quantity;
    }

    public double addQuantity(double amount){
        this.quantity += amount;
        return this.quantity;
    }

    public double getQuantity() {
        return quantity;
    }

    public void setQuantity(double quantity) {
        this.quantity = quantity;
    }


    public Integer getId() {
        return id;
    }

    public void setId(Integer id) {
        this.id = id;
    }

    public String getName() {
        return name;
    }

    public void setName(String name) {
        this.name = name;
    }

    public double getPrice() {
        return price;
    }

    public void setPrice(double price) {
        this.price = price;
    }

    public Integer getUserId() {
        return userId;
    }

    public void setUserId(Integer userId) {
        this.userId = userId;
    }
}
